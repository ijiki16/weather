package de.beowulf.wetter

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.LinearLayout
import de.beowulf.wetter.adapter.MyListDayCitiesAdapter
import de.beowulf.wetter.databinding.WeatherForecastBinding
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


class DayFragment : Fragment() {

    private lateinit var binding: WeatherForecastBinding

    private val dayDate = arrayOfNulls<String>(8)
    private val statusDay = arrayOfNulls<Int>(8)
    private val statusDayText = arrayOfNulls<String>(8)
    private val minTemp = arrayOfNulls<String>(8)
    private val maxTemp = arrayOfNulls<String>(8)
    private val sunrise = arrayOfNulls<String>(8)
    private val sunset = arrayOfNulls<String>(8)
    private val wind = arrayOfNulls<String>(8)
    private val pressure = arrayOfNulls<String>(8)
    private val humidity = arrayOfNulls<String>(8)
    private val rainSnow = arrayOfNulls<String>(8)
    private val rainSnowType = arrayOfNulls<String>(8)
    private val gf = GlobalFunctions()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = WeatherForecastBinding.inflate(layoutInflater)
        val view: View = binding.root

        gf.initializeContext(context!!)

        val jsonObj = gf.result()

        binding.ListView.onItemClickListener =
            AdapterView.OnItemClickListener { _, v, _, _ ->
                val moreInfo = v.findViewById<LinearLayout>(R.id.dayMoreInfo)
                if (moreInfo.visibility == View.GONE) {
                    moreInfo.visibility = View.VISIBLE
                } else {
                    moreInfo.visibility = View.GONE
                }
            }

        for (i: Int in 0..7) {
            val daily: JSONObject = jsonObj.getJSONArray("daily").getJSONObject(i)
            val dailyTemp: JSONObject = daily.getJSONObject("temp")
            val dailyWeather: JSONObject = daily.getJSONArray("weather").getJSONObject(0)

            /* Populating extracted data into our views */
            dayDate[i] = SimpleDateFormat(getString(R.string.daydate), Locale.ROOT).format(
                Date(
                    daily.getLong("dt") * 1000
                )
            )
            if (i == 1) {
                val date: String =
                    SimpleDateFormat(
                        getString(R.string.daydate),
                        Locale.ROOT
                    ).format(Date())
                if (dayDate[0] == date) {
                    dayDate[0] = getString(R.string.today)
                    dayDate[1] = getString(R.string.tomorrow)
                }
            }
            val windDegree: Int = daily.getInt("wind_deg")
            val windDouble: Double = daily.getDouble("wind_speed")
            val precipitation: Double = daily.getDouble("pop") * 100
            statusDay[i] = gf.icon(dailyWeather.getString("icon"))
            statusDayText[i] = dailyWeather.getString("description")
            minTemp[i] = gf.convertTemp(dailyTemp.getDouble("min"), 0)
            maxTemp[i] = gf.convertTemp(dailyTemp.getDouble("max"), 0)
            sunrise[i] = SimpleDateFormat(gf.getTime(), Locale.ROOT).format(
                Date(
                    daily.getLong("sunrise") * 1000
                )
            )
            sunset[i] = SimpleDateFormat(gf.getTime(), Locale.ROOT).format(
                Date(
                    daily.getLong("sunset") * 1000
                )
            )
            wind[i] = gf.convertSpeed(windDouble) + " (${gf.degToCompass(windDegree)})"
            pressure[i] = daily.getString("pressure") + "hPa"
            humidity[i] = daily.getString("humidity") + "%"
            when {
                daily.has("rain") -> {
                    rainSnow[i] = gf.convertRain(daily.getDouble("rain"))
                    rainSnowType[i] = "rain"
                }
                daily.has("snow") -> {
                    rainSnow[i] = gf.convertRain(daily.getDouble("snow"))
                    rainSnowType[i] = "snow"
                }
                else -> {
                    rainSnow[i] = gf.convertRain(0.0)
                    rainSnowType[i] = "rain"
                }
            }
            rainSnow[i] = rainSnow[i] + " (${precipitation.toString().split(".")[0]}%)"
        }

        val myListAdapter = MyListDayCitiesAdapter(
            activity!!,
            dayDate,
            statusDay,
            statusDayText,
            minTemp,
            maxTemp,
            sunrise,
            sunset,
            wind,
            pressure,
            humidity,
            rainSnow,
            rainSnowType
        )
        binding.ListView.adapter = myListAdapter

        return view
    }
}