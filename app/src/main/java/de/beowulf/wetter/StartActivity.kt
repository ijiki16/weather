package de.beowulf.wetter

import android.app.AlertDialog
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.ContextThemeWrapper
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import de.beowulf.wetter.databinding.ActivityStartBinding
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import java.util.concurrent.Executors

class StartActivity : AppCompatActivity() {

    private lateinit var binding: ActivityStartBinding

    private val gf = GlobalFunctions()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStartBinding.inflate(layoutInflater)
        setContentView(binding.root)

        gf.initializeContext(this)

        val settings: SharedPreferences = getSharedPreferences("de.beowulf.wetter", 0)

        binding.CreatedBy.movementMethod = LinkMovementMethod.getInstance()
        binding.ReportError.movementMethod = LinkMovementMethod.getInstance()
        binding.OWM.movementMethod = LinkMovementMethod.getInstance()

        var lat: String = settings.getString("lat", "")!!
        var city: String = settings.getString("city", "")!!
        var unitTemp: Int = settings.getInt("unitTemp", 0)
        var unitSpeed: Int = settings.getInt("unitSpeed", 0)
        var unitDistance: Int = settings.getInt("unitDistance", 0)
        var timeFormat: Boolean = settings.getBoolean("24hTime", false)
        var api: String = settings.getString("api", "")!!
        val change: Boolean = intent.getBooleanExtra("change", false)

        val unitsTemp = resources.getStringArray(R.array.unitsTemp)
        val unitsSpeed = resources.getStringArray(R.array.unitsSpeed)
        val unitsDistance = resources.getStringArray(R.array.unitsDistance)
        binding.UnitTemp.adapter = ArrayAdapter(this, R.layout.spinner, unitsTemp)
        binding.UnitSpeed.adapter = ArrayAdapter(this, R.layout.spinner, unitsSpeed)
        binding.UnitDistance.adapter = ArrayAdapter(this, R.layout.spinner, unitsDistance)

        if (change) {
            binding.City.setText(city)
            binding.UnitTemp.setSelection(unitTemp)
            binding.UnitSpeed.setSelection(unitSpeed)
            binding.UnitDistance.setSelection(unitDistance)
            if (api != getString(R.string.standardKey))
                binding.Api.setText(api)
            binding.TimeFormat.isChecked = timeFormat
        }

        binding.ApiText.setOnClickListener {
            val uriUrl: Uri = Uri.parse("https://home.openweathermap.org/api_keys")
            val launchBrowser = Intent(Intent.ACTION_VIEW, uriUrl)
            startActivity(launchBrowser)
        }

        if (lat != "" && !change) { // When already initialized load data and start the MainActivity
            binding.loader.visibility = View.VISIBLE
            binding.mainContainer.visibility = View.GONE

            startMain(false)

        } else binding.Submit.setOnClickListener { // When not already initialized, set Submit-Button OnClickListener and check the data
            binding.loader.visibility = View.VISIBLE
            binding.mainContainer.visibility = View.GONE
            val executor = Executors.newScheduledThreadPool(5)

            doAsync(executorService = executor) {
                var error = 0
                val result: JSONArray = try {
                    city = binding.City.text.toString()
                    api = binding.Api.text.toString()
                    if (api == "")
                        api = getString(R.string.standardKey)
                    JSONArray(
                        URL("https://api.openweathermap.org/geo/1.0/direct?q=$city&limit=10&appid=$api").readText(Charsets.UTF_8)
                    )
                } catch (e: Exception) {
                    try {
                        with(URL("https://api.openweathermap.org/geo/1.0/direct?q=$city&limit=10&appid=$api").openConnection() as HttpURLConnection) {
                            requestMethod = "GET"
                            error = if (responseCode == 429 || responseCode == 401) {
                                1
                            } else {
                                2
                            }
                        }
                    } catch (e: Exception) {
                        error = 3
                    }
                    JSONArray("[]")
                }
                uiThread {
                    if (error == 0 && result.length() != 0) {
                        var i = 0
                        val cities = arrayOfNulls<String>(result.length())
                        while (i < result.length())
                        {
                            if (result.getJSONObject(i).has("state")) {
                                cities[i] = result.getJSONObject(i).getString("name") +
                                        ", " + result.getJSONObject(i).getString("state") +
                                        ", " + result.getJSONObject(i).getString("country")
                            } else {
                                cities[i] = result.getJSONObject(i).getString("name") +
                                        ", " + result.getJSONObject(i).getString("country")
                            }
                            i++
                        }

                        AlertDialog.Builder(ContextThemeWrapper(this@StartActivity, R.style.Theme_AppCompat_Dialog_Alert))
                            .setTitle(R.string.choose_city)
                            .setItems(cities) { _, selected ->
                                city = cities[selected].toString()
                                lat = result.getJSONObject(selected).getString("lat")
                                val lon: String = result.getJSONObject(selected).getString("lon")

                                unitTemp = binding.UnitTemp.selectedItemPosition
                                unitSpeed = binding.UnitSpeed.selectedItemPosition
                                unitDistance = binding.UnitDistance.selectedItemPosition
                                timeFormat = binding.TimeFormat.isChecked

                                // Save all needed values
                                settings.edit()
                                    .putString("lon", lon)
                                    .putString("lat", lat)
                                    .putString("city", city)
                                    .putInt("unitTemp", unitTemp)
                                    .putInt("unitSpeed", unitSpeed)
                                    .putInt("unitDistance", unitDistance)
                                    .putBoolean("24hTime", timeFormat)
                                    .putString("api", api)
                                    .apply()

                                startMain(true)
                            }
                            .setNegativeButton(R.string.abort) { dialog, _ ->
                                dialog.cancel()
                                binding.loader.visibility = View.GONE
                                binding.mainContainer.visibility = View.VISIBLE
                            }
                            .setCancelable(false)
                            .show()
                    } else {
                        binding.loader.visibility = View.GONE
                        binding.mainContainer.visibility = View.VISIBLE
                        when (error) {
                            1 -> {
                                Toast.makeText(this@StartActivity, R.string.error_1_start, Toast.LENGTH_SHORT).show()
                            }
                            0, 2 -> {
                                Toast.makeText(this@StartActivity,  R.string.error_2_start, Toast.LENGTH_SHORT).show()
                            }
                            else -> {
                                Toast.makeText(this@StartActivity,  R.string.error_3_start, Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                }
            }
        }
    }

    private fun startMain(mustSuccess: Boolean) {
        // Load new data
        val executor = Executors.newScheduledThreadPool(5)

        doAsync(executorService = executor) {
            val result: String? = try {
                URL(gf.url("normal", "")).readText(Charsets.UTF_8)
            } catch (e: Exception) {
                null
            }
            uiThread {
                if (result != null || !mustSuccess) { // when loaded data isn't empty, or old data is available, start the Main activity
                    if (result != null) {
                        gf.setResult(result)
                        gf.setInitialized(true)
                    } else {
                        Toast.makeText(this@StartActivity, R.string.error_occurred, Toast.LENGTH_LONG).show()
                    }
                    val intent = Intent(this@StartActivity, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                } else { // else display error message
                    Toast.makeText(this@StartActivity, R.string.error_3_start, Toast.LENGTH_LONG).show()
                    binding.loader.visibility = View.GONE
                    binding.mainContainer.visibility = View.VISIBLE
                }
            }
        }
    }
}