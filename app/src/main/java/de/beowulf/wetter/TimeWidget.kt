package de.beowulf.wetter

import android.app.AlarmManager
import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.AlarmClock
import android.util.TypedValue
import android.view.View
import android.widget.RemoteViews
import androidx.core.content.ContextCompat.getSystemService
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.json.JSONObject
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors

class TimeWidget : AppWidgetProvider() {

    private val gf = GlobalFunctions()

    override fun onReceive(context: Context, intent: Intent) {
        super.onReceive(context, intent)

        gf.initializeContext(context)

        val appWidgetManager = AppWidgetManager.getInstance(context)
        val thisAppWidgetComponentName = ComponentName(context.packageName, javaClass.name)
        val appWidgetIds: IntArray =
            appWidgetManager.getAppWidgetIds(thisAppWidgetComponentName)

        if (gf.getInitialized()) {
            if ("android.appwidget.action.APPWIDGET_UPDATE" == intent.action ||
                    "AUTO_UPDATE" == intent.action) {
                val executor = Executors.newScheduledThreadPool(5)

                context.doAsync(executorService = executor) {
                    val result: String? = try {
                        URL(gf.url("normal", "")).readText(Charsets.UTF_8)
                    } catch (e: Exception) {
                        null
                    }
                    uiThread {
                        if (result != null) {
                            gf.setResult(result)
                        }
                        // There may be multiple widgets active, so update all of them
                        for (appWidgetId in appWidgetIds) {
                            updateTimeWidget(context, appWidgetManager, appWidgetId)
                        }
                    }
                }
            } else {
                // There may be multiple widgets active, so update all of them
                for (appWidgetId in appWidgetIds) {
                    updateTimeWidget(context, appWidgetManager, appWidgetId)
                }
            }
        }
    }

    override fun onAppWidgetOptionsChanged(
        context: Context,
        appWidgetManager: AppWidgetManager,
        appWidgetId: Int,
        newOptions: Bundle
    ) {
        val views = RemoteViews(context.packageName, R.layout.time_widget)

        resizeWidget(newOptions, views)

        gf.initializeContext(context)

        if (gf.getInitialized()) {
            updateTimeWidget(context, appWidgetManager, appWidgetId)
        } else {
            //Start app, when you click on the widget and when API level 19 or higher, open Alarm Settings, when you click on the clock
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                val configIntent = Intent(AlarmClock.ACTION_SHOW_ALARMS)
                val configPendingIntent = PendingIntent.getActivity(context, 0, configIntent, 0)
                views.setOnClickPendingIntent(R.id.Clock, configPendingIntent)
            }
            val configIntent = Intent(context, StartActivity::class.java)
            val configPendingIntent = PendingIntent.getActivity(context, 0, configIntent, 0)
            views.setOnClickPendingIntent(R.id.TimeWidget, configPendingIntent)

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
        val appWidgetAlarm = WidgetUpdater(context.applicationContext, "TimeWidget")
        appWidgetAlarm.startAlarm()
    }

    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
        val appWidgetManager = AppWidgetManager.getInstance(context)
        val thisAppWidgetComponentName = ComponentName(context.packageName, javaClass.name)
        val appWidgetIds = appWidgetManager.getAppWidgetIds(thisAppWidgetComponentName)
        if (appWidgetIds.isEmpty()) {
            // stop alarm
            val appWidgetAlarm = WidgetUpdater(context.applicationContext, "TimeWidget")
            appWidgetAlarm.stopAlarm()
        }
    }

    companion object {
        @JvmField
        var ACTION_AUTO_UPDATE: String = "AUTO_UPDATE"
    }
}

internal fun updateTimeWidget(
    context: Context,
    appWidgetManager: AppWidgetManager,
    appWidgetId: Int
) {
    val gf = GlobalFunctions()
    val views = RemoteViews(context.packageName, R.layout.time_widget)

    gf.initializeContext(context)
    val jsonObj = gf.result()

    //get data
    val current: JSONObject = jsonObj.getJSONObject("current")
    val currentWeather: JSONObject = current.getJSONArray("weather").getJSONObject(0)

    val nextAlarm: String? =
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP && getSystemService(
                context,
                AlarmManager::class.java
            )?.nextAlarmClock?.triggerTime != null
        ) {
            SimpleDateFormat("E ${gf.getTime()}", Locale.ROOT).format(
                Date(
                    getSystemService(context, AlarmManager::class.java)!!.nextAlarmClock.triggerTime
                )
            )
        } else {
            null
        }
    val image: Int = gf.icon(currentWeather.getString("icon"))
    val actualTemp: String = gf.convertTemp(current.getDouble("temp"), 0)
    val statusText: String = currentWeather.getString("description")
    val wind: String =
        gf.convertSpeed(current.getDouble("wind_speed")) + " (${
            gf.degToCompass(current.getInt("wind_deg"))
        })"

    var rainSnow: String
    var type = "snow"
    when {
        current.has("rain") -> {
            rainSnow = gf.convertRain(current.getJSONObject("rain").getDouble("1h"))
            type = "rain"
        }
        current.has("snow") -> {
            rainSnow = gf.convertRain(current.getJSONObject("snow").getDouble("1h"))
        }
        else -> {
            rainSnow = gf.convertRain(0.0)
            type = "rain"
        }
    }
    if (type == "rain") {
        views.setViewVisibility(R.id.Rain, View.VISIBLE)
        views.setViewVisibility(R.id.Snow, View.GONE)
    } else {
        views.setViewVisibility(R.id.Rain, View.GONE)
        views.setViewVisibility(R.id.Snow, View.VISIBLE)
    }
    val precipitation: Double =
        jsonObj.getJSONArray("hourly").getJSONObject(0).getDouble("pop") * 100
    rainSnow += " (${precipitation.toString().split(".")[0]}%)"

    //set View
    if (nextAlarm == null) {
        views.setViewVisibility(R.id.AlarmIcon, View.GONE)
        views.setViewVisibility(R.id.nextAlarm, View.GONE)
    } else {
        views.setViewVisibility(R.id.AlarmIcon, View.VISIBLE)
        views.setViewVisibility(R.id.nextAlarm, View.VISIBLE)
        views.setTextViewText(R.id.nextAlarm, nextAlarm)
    }
    views.setImageViewResource(R.id.Status_Image, image)
    views.setTextViewText(R.id.actualTemp, actualTemp)
    views.setTextViewText(R.id.Status_Text, statusText)
    views.setTextViewText(R.id.Wind, wind)
    views.setTextViewText(R.id.RainSnow, rainSnow)

    val appWidgetOptions = appWidgetManager.getAppWidgetOptions(appWidgetId)
    resizeWidget(appWidgetOptions, views)

    //Start app, when you click on the widget and when API level 19 or higher, open Alarm Settings, when you click on the clock
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
        val configIntent = Intent(AlarmClock.ACTION_SHOW_ALARMS)
        val configPendingIntent = PendingIntent.getActivity(context, 0, configIntent, 0)
        views.setOnClickPendingIntent(R.id.Clock, configPendingIntent)
    }
    val configIntent = Intent(context, StartActivity::class.java)
    val configPendingIntent = PendingIntent.getActivity(context, 0, configIntent, 0)
    views.setOnClickPendingIntent(R.id.TimeWidget, configPendingIntent)

    // Instruct the widget manager to update the widget
    appWidgetManager.updateAppWidget(appWidgetId, views)
}

private fun resizeWidget(appWidgetOptions: Bundle, views: RemoteViews) {
    val minWidth: Int = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_WIDTH)
    /*val maxWidth: Int = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_WIDTH)
    val minHeight: Int = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT)*/
    val maxHeight: Int = appWidgetOptions.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT)

    when {
        minWidth < 250 -> {
            views.setViewVisibility(R.id.WindRain, View.GONE)

        }
        else -> {
            views.setViewVisibility(R.id.WindRain, View.VISIBLE)
        }
    }
    when {
        maxHeight < 110 -> {
            views.setTextViewTextSize(R.id.Clock, TypedValue.COMPLEX_UNIT_SP, 45F)
        }
        else -> {
            views.setTextViewTextSize(R.id.Clock, TypedValue.COMPLEX_UNIT_SP, 70F)
        }
    }
}